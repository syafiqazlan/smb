import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';


/*
  Generated class for the UserDataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserDataProvider {

  private name: string;

  constructor(public http: Http, private storage: Storage) {
    console.log('Hello UserDataProvider Provider');

    this.getName().then((name) => {
      this.name = name;
    });

  }

 public getName(): Promise<string> {
    if (this.name) {
      return Promise.resolve(this.name);
    }
    else {
      return this.storage.get('userData').then((name) => {
        this.name = name;
        return name;
      });
    }
  }


}
