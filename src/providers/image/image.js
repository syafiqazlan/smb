var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import 'rxjs/add/operator/map';
/*
  Generated class for the ImageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var ImageProvider = /** @class */ (function () {
    function ImageProvider() {
    }
    ImageProvider.prototype.uploadImage = function (image, userId) {
        var storageRef = firebase.storage().ref();
        var imageName = this.generateUUID();
        var imageRef = storageRef.child(userId + "/" + imageName + ".jpg");
        return imageRef.putString(image, 'data_url');
    };
    ImageProvider.prototype.getImage = function (userId, imageId) {
        var storageRef = firebase.storage().ref();
        var imageRef = storageRef.child(userId + "/" + imageId);
        return imageRef.getDownloadURL();
    };
    ImageProvider.prototype.generateUUID = function () {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    };
    ImageProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [])
    ], ImageProvider);
    return ImageProvider;
}());
export { ImageProvider };
//# sourceMappingURL=image.js.map