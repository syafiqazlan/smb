import { Component } from '@angular/core';
import { NavController, Events, LoadingController} from 'ionic-angular';
import { AlertController } from 'ionic-angular';

import { Camera, CameraOptions } from '@ionic-native/camera';
import { Transfer, FileUploadOptions, TransferObject } from '@ionic-native/transfer';

import { CreatePage } from '../create/create';
import { DetailsPage } from '../details/details';

import { Storage } from '@ionic/storage';


import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { UserDataProvider } from '../../providers/user-data/user-data';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html' 
})
export class HomePage  {

	items : any;
  username : any;
  email : any;
  userId : any;

  constructor(public events: Events,  public loadingCtrl: LoadingController, public UserDataProvider : UserDataProvider, public navCtrl: NavController, public alertCtrl: AlertController, public http: Http, private transfer: Transfer, private camera: Camera,  public storage: Storage) {

  	// this.http.get('http://localhost/smb/home.php').map(res => res.json()).subscribe(data => {
   //      this.posts = data.data.children;
   //  });
           //console.log("u data home: ", UserDataProvider.getName());
events.publish('user:check');


    //get admin name
  let loading = this.loadingCtrl.create({
    content: 'Loading...'
  });

  // loading.present();

  // setTimeout(() => {
  //   loading.dismiss();
  // }, 2700);

        this.storage.get('userData').then((val) => {
          if(val == null)
          {
            console.log("local NULLL");
          }

          else{
              loading.present();
            // this.userId = val.userId;
            console.log("uu iddd",val.userId);
  this.http.get('http://localhost/smb/home.php?userId='+val.userId).map(res => res.json()).subscribe(data => {
     this.items  = data;  
     console.log("homee999 :::", this.items);  
                 this.storage.set('homeData', data);   
});
      loading.dismiss();
          }
        });  


 //this.posts = null;


  }

   getItems(ev) {
    // Reset items back to all of the items
    let val = ev.target.value;

// if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.billTitle.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
    else
    {


        this.storage.get('homeData').then((val) => {

        this.items = val;

        }); 
      
    }
  }

    getUserData()
  {
           // console.log("u data home: ", UserDataProvider.getName());

       //  this.storage.get('username').then((val) => {

       //  console.log('userData username', val);
       // // console.log('userData email', val.email);

       //  this.username = val;
       //  //this.email = val.email;
       //  });          

  }

  goCreate()
  {
  	this.navCtrl.push(CreatePage);
  }

  goDetails(billId, billAdmin, billSize, billTitle)
  {
        console.log("asasssssssssssss"+billId+billAdmin+billAdmin+billSize+billTitle);
  	  	this.navCtrl.push(DetailsPage, 
          {
            billId: billId,
            billAdmin: billAdmin,
            billSize: billSize,
            billTitle: billTitle
          });
  }

   showConfirm() {
    let confirm = this.alertCtrl.create({
      title: 'Join a Bill',
      message: 'Enter the bill ID that you want to join',
      
      inputs: [
        {
          name: 'billId',
          placeholder: 'Example: BS0101'
        },
      ],

      buttons: [
        {
          text: 'Cancel',
          handler: () => {
          }
        },
        {
          text: 'Ok',
          handler:  data => {
           // console.log(JSON.stringify(data)); //to see the object
           var billId = data.billId;
           console.log(data.billId);

           var enteredId = billId.substr(0, 4);

           if (enteredId == "BS01")
           {
             var requestedId = billId.substr(4, 4);

             //check bill id exist
             this.http.get('http://localhost/smb/checkBillExist.php?billId='+requestedId).map(res => res.json()).subscribe(data => {
             var status  = data[0].status;

             if (status == "exist")
             {
               this.http.get('http://localhost/smb/getBillGroup.php?billId='+requestedId).map(res => res.json()).subscribe(data => {
               var amountEach  = data[0].amountEach;
               var billSize = data[0].billSize;

               var currentLength = parseInt(data.length)+1;

               if (currentLength > billSize)
               {
                  let alert = this.alertCtrl.create({
                  title: 'Failed',
                  subTitle: 'This bill is full, please ask the admin to change its size',
                  buttons: ['OK']
                  });
                  alert.present();
                  console.log("current length = "+currentLength+"~~~bill size = ", billSize);
               }
               else
               {
                 console.log("ok, blh masuk, x penuh");
                 //insert into billgrooup
                  this.storage.get('userData').then((val) => {
                   var currentUser = val.userId;

                   console.log(requestedId+currentUser+amountEach);

                  this.http.get('http://localhost/smb/joinBill.php?billId='+requestedId+'&userId='+currentUser+'&amountEach='+amountEach).map(res4 => res4.json()).subscribe(data4 => {   
                  
                                       console.log("user yg nk masukkk:", data4[0].status);
                  let alert = this.alertCtrl.create({
                  title: 'Success',
                  subTitle: 'You have successfully joined this bill',
                  buttons: ['OK']
                  });
                  alert.present();
                  });                 

                  });  

               }
               
               }); 
             }
             else
             {
              let alert = this.alertCtrl.create({
              title: 'Failed',
              subTitle: 'Unable to find the bill id, please enter it again',
              buttons: ['OK']
              });
              alert.present();
             }

             });  

            
           }

           else
           {

              let alert = this.alertCtrl.create({
              title: 'Failed',
              subTitle: 'Incorrect Format of Bill Id',
              buttons: ['OK']
              });
              alert.present();

           }

          }
        }
      ]
    });
    confirm.present();
  }

  upload()
    {
      
  //      let options = {

  //          quality: 100
  //           };


  //     this.camera.getPicture(options).then((imageData) => {
  //      // imageData is either a base64 encoded string or a file URI
  //      // If it's base64:

  //      alert(imageData);
  //    const fileTransfer: TransferObject = this.transfer.create();

  //     let options1: FileUploadOptions = {
  //        fileKey: 'file',
  //        fileName: 'filename',
  //        headers: {Connection: "close"}
      
  //     }

  // fileTransfer.upload(imageData, 'http://localhost/smb/upload.php', options1)
  //  .then((data) => {
  //    // success
  //    alert("success");
  //  }, (err) => {
  //    // error
  //    alert("error"+JSON.stringify(err));
  //  });


  //   });

 
}

}

