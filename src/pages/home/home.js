var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { Transfer } from '@ionic-native/transfer';
import { CreatePage } from '../create/create';
import { DetailsPage } from '../details/details';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, alertCtrl, http, transfer, camera) {
        // this.http.get('http://localhost/smb/home.php').map(res => res.json()).subscribe(data => {
        //      this.posts = data.data.children;
        //  });
        var _this = this;
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.http = http;
        this.transfer = transfer;
        this.camera = camera;
        this.posts = null;
        this.http.get('http://localhost/smb/home.php').map(function (res) { return res.json(); }).subscribe(function (data) {
            _this.posts = data;
        });
    }
    HomePage.prototype.goCreate = function () {
        this.navCtrl.push(CreatePage);
    };
    HomePage.prototype.goDetails = function () {
        this.navCtrl.push(DetailsPage);
    };
    HomePage.prototype.showConfirm = function () {
        var confirm = this.alertCtrl.create({
            title: 'Join a Bill',
            message: 'Enter the bill ID that you want to join',
            inputs: [
                {
                    name: 'Bill id',
                    placeholder: 'Bill id'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function () {
                    }
                },
                {
                    text: 'Ok',
                    handler: function () {
                    }
                }
            ]
        });
        confirm.present();
    };
    HomePage.prototype.upload = function () {
        var _this = this;
        var options = {
            quality: 100
        };
        this.camera.getPicture(options).then(function (imageData) {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64:
            alert(imageData);
            var fileTransfer = _this.transfer.create();
            var options1 = {
                fileKey: 'file',
                fileName: 'filename',
                headers: { Connection: "close" }
            };
            fileTransfer.upload(imageData, 'http://localhost/smb/upload.php', options1)
                .then(function (data) {
                // success
                alert("success");
            }, function (err) {
                // error
                alert("error" + JSON.stringify(err));
            });
        });
    };
    HomePage = __decorate([
        Component({
            selector: 'page-home',
            templateUrl: 'home.html'
        }),
        __metadata("design:paramtypes", [NavController, AlertController, Http, Transfer, Camera])
    ], HomePage);
    return HomePage;
}());
export { HomePage };
//# sourceMappingURL=home.js.map