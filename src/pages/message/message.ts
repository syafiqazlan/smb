import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
/**
 * Generated class for the MessagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-message',
  templateUrl: 'message.html',
})
export class MessagePage {

public items:any;
  constructor(public events: Events,  public loadingCtrl: LoadingController, public navCtrl: NavController, public navParams: NavParams, public http: Http, public storage: Storage) {

events.publish('user:check');

  let loading = this.loadingCtrl.create({
    content: 'Loading...'
  });

  loading.present();


  setTimeout(() => {
    loading.dismiss();
  }, 3000);
     //get userId
     this.storage.get('userData').then((val) => {

  	 this.http.get('http://localhost/smb/getMessage.php?userId='+val.userId).map(res => res.json()).subscribe(data => {
     this.items = data;
});
    
        }); 
  //loading.dismiss();



  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MessagePage');
  }

}
