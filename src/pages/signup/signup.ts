import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';

import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

	data = {};
	status : any;

  constructor(public navCtrl: NavController, public navParams: NavParams,  public http: Http, public alertCtrl: AlertController, public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  doRegister()
  {
      var username = this.data['username'];
      var password = this.data['password'];
      var email = this.data['email'];
      var phone = this.data['phone'];

  if (username != undefined && password != undefined && email != undefined && phone != undefined)
  {

    const loading = this.loadingCtrl.create({
        content: 'Loading ...'
      });

      loading.present();

    this.http.get('http://localhost/smb/register.php?username='+username+'&password='+password+'&email='+email+'&phone='+phone).map(res => res.json()).subscribe(data => {

      if (data[0].status == "pass")
          {
            loading.dismiss();          
            console.log('pass');
            this.showAlertCorrect();
          }

      else
          {
            this.showAlertIncorrect();
            loading.dismiss();  
            console.log('x pass');
          }

      });
  }

  else
  {
     this.showAlertIncorrect();
    console.log('x masuk kosong je');
  }

  }

  showAlertIncorrect() 
  {
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: 'Please try again',
      buttons: ['Ok']
    });
    alert.present();
  }

  showAlertCorrect() 
  {
    let alert = this.alertCtrl.create({
      title: 'Success',
      subTitle: 'Your accound have been created',
      buttons: 
      [
        {
          text: 'Ok',
          handler: () => {
             this.navCtrl.pop();
            console.log('ok clicked');
          }
        },
      ]
    });
    alert.present();
  }

}
