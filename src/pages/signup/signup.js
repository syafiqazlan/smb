var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SignupPage = /** @class */ (function () {
    function SignupPage(navCtrl, navParams, http, alertCtrl, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.data = {};
    }
    SignupPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SignupPage');
    };
    SignupPage.prototype.doRegister = function () {
        var _this = this;
        var username = this.data['username'];
        var password = this.data['password'];
        var email = this.data['email'];
        var phone = this.data['phone'];
        if (username != undefined && password != undefined && email != undefined && phone != undefined) {
            var loading_1 = this.loadingCtrl.create({
                content: 'Loading ...'
            });
            loading_1.present();
            this.http.get('http://localhost/smb/register.php?username=' + username + '&password=' + password + '&email=' + email + '&phone=' + phone).map(function (res) { return res.json(); }).subscribe(function (data) {
                if (data[0].status == "pass") {
                    loading_1.dismiss();
                    console.log('pass');
                    _this.showAlertCorrect();
                }
                else {
                    _this.showAlertIncorrect();
                    loading_1.dismiss();
                    console.log('x pass');
                }
            });
        }
        else {
            this.showAlertIncorrect();
            console.log('x masuk kosong je');
        }
    };
    SignupPage.prototype.showAlertIncorrect = function () {
        var alert = this.alertCtrl.create({
            title: 'Error',
            subTitle: 'Please try again',
            buttons: ['Ok']
        });
        alert.present();
    };
    SignupPage.prototype.showAlertCorrect = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Success',
            subTitle: 'Your accound have been created',
            buttons: [
                {
                    text: 'Ok',
                    handler: function () {
                        _this.navCtrl.pop();
                        console.log('ok clicked');
                    }
                },
            ]
        });
        alert.present();
    };
    SignupPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-signup',
            templateUrl: 'signup.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, Http, AlertController, LoadingController])
    ], SignupPage);
    return SignupPage;
}());
export { SignupPage };
//# sourceMappingURL=signup.js.map