import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { HomePage } from '../home/home';

import { Storage } from '@ionic/storage';
/**
 * Generated class for the CreatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create',
  templateUrl: 'create.html',
})
export class CreatePage {

  datas = {};
  billType:any = "one-off";
  userId:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public alertCtrl: AlertController, public storage: Storage) {
  }

  public event = {
    month: '2017-01-01',
    timeStarts: '07:43',
    timeEnds: '1990-02-20'
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreatePage');
  }

select(item) {
   this.billType = item;
   console.log("slcted item, ", this.billType);
}

  doCreate()
  {
    var billTitle = this.datas['billTitle'];
    var billDescription = this.datas['billDescription'];
    var billDue = this.datas['billDue'];
    var billSize = this.datas['billSize'];
    var billAmount = this.datas['billAmount'];
    var billType = this.billType;
    console.log(billTitle+billType+billDescription+billDue+billAmount);

    this.storage.get('userData').then((val) => {
    this.userId = val.userId;   

    if (billTitle != undefined && billDescription != undefined && billDue != undefined && billAmount != undefined && billTitle != undefined && billSize != undefined)
    {
      this.http.get('http://localhost/smb/createBill.php?billTitle='+billTitle+'&billDescription='+billDescription+'&billDue='+billDue+'&billAmount='+billAmount+'&billType='+billType+'&userId='+this.userId+'&billSize='+billSize).map(res => res.json()).subscribe(data => {

      console.log("data create", data);
      if (data[0].status == "pass")
           {         
             console.log('pass');
                let alert = this.alertCtrl.create({
      title: 'Success!',
      subTitle: 'You bill have been created',
      message: '<b>Share</b> this bill id to your another '+billSize+' members through <b>bill details</b> to add them',
      buttons: ['OK']
    });
    alert.present();
            this.navCtrl.push(HomePage);
            this.navCtrl.setRoot(HomePage);
           }

      else
           {
             // this.showAlertIncorrect();
             console.log('x pass');
           }

       });
   }
   else{
                let alert = this.alertCtrl.create({
      title: 'Fail!',
      subTitle: 'Please try again',
      buttons: ['OK']
    });
    alert.present();
   }           
                 
    }); 



    }
  }

