var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, AlertController, LoadingController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { SignupPage } from '../signup/signup';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, menu, http, alertCtrl, loadingCtrl, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menu = menu;
        this.http = http;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.datas = {};
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.ionViewDidEnter = function () {
        this.menu.swipeEnable(false);
        //  	this.navCtrl.setRoot(HomePage);
    };
    LoginPage.prototype.doLogin = function () {
        var _this = this;
        //var status;
        this.posts = null;
        var username = this.datas['username'];
        var password = this.datas['password'];
        if (username != undefined && password != undefined) {
            var loading_1 = this.loadingCtrl.create({
                content: 'Loggin in...'
            });
            loading_1.present();
            this.http.get('http://localhost/smb/login.php?username=' + username + '&password=' + password).map(function (res) { return res.json(); }).subscribe(function (data) {
                if (data[0].status == "pass") {
                    var userId = data[0].userId;
                    var username = data[0].username;
                    var phone = data[0].phone;
                    var email = let, userData = {
                        userId: data[0].userId,
                        username: data[0].username,
                        phone: data[0].phone,
                        email: data[0].email
                    };
                    // this.storage.set('userId', data[0].userId);
                    // this.storage.set('username', data[0].username);
                    // this.storage.set('phone', data[0].phone);
                    // this.storage.set('email', data[0].email);
                    _this.storage.set('userData', userData);
                    _this.storage.get('userData').then(function (val) {
                        console.log('userData', val);
                    });
                    loading_1.dismiss();
                    console.log('pass');
                    _this.navCtrl.push(HomePage);
                    _this.navCtrl.setRoot(HomePage);
                }
                else {
                    _this.showAlertIncorrect();
                    loading_1.dismiss();
                    console.log('x pass');
                }
            });
        }
        else {
            this.showAlertIncorrect();
            console.log('x masuk kosong je');
        }
    };
    LoginPage.prototype.doRegister = function () {
        this.navCtrl.push(SignupPage);
    };
    LoginPage.prototype.showAlertIncorrect = function () {
        var alert = this.alertCtrl.create({
            title: 'Error',
            subTitle: 'Incorrect Username or Password',
            buttons: ['OK']
        });
        alert.present();
    };
    LoginPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-login',
            templateUrl: 'login.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, MenuController, Http, AlertController, LoadingController, Storage])
    ], LoginPage);
    return LoginPage;
}());
export { LoginPage };
//# sourceMappingURL=login.js.map