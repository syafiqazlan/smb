import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, AlertController, LoadingController, ModalController } from 'ionic-angular';

import { HomePage } from '../home/home';
import { SignupPage } from '../signup/signup';
import { ForgetPage } from '../forget/forget';

import { Storage } from '@ionic/storage';

import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

	posts: any;
	datas = {};
	status : any;

  constructor(public navCtrl: NavController, public modalCtrl: ModalController, public navParams: NavParams, private menu: MenuController, public http: Http, public alertCtrl: AlertController, public loadingCtrl: LoadingController, public storage: Storage) {
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

 ionViewDidEnter() {
    this.menu.swipeEnable(false);
    //  	this.navCtrl.setRoot(HomePage);
  }

  doLogin()
  {
  	//var status;
  	this.posts = null;
  	var username = this.datas['username'];
  	var password = this.datas['password'];

 	if (username != undefined && password != undefined)
 	{

		const loading = this.loadingCtrl.create({
		    content: 'Loggin in...'
		  });

  		loading.present();

 		this.http.get('http://localhost/smb/login.php?username='+username+'&password='+password).map(res => res.json()).subscribe(data => {

      // console.log(data);
			if (data[0].status == "pass")
			   	{
            this.storage.set('userData', data[0]);

            // this.storage.get('userData').then((val) => {
            //     console.log('userData', val);
            //   });   					
            loading.dismiss();					
			   		console.log('pass');
			   		this.navCtrl.push(HomePage);
			  		this.navCtrl.setRoot(HomePage);
			   	}

			else
			   	{
			   		this.showAlertIncorrect();
			   		loading.dismiss();	
			   		console.log('x pass');
		   		}

	   	});
 	}

 	else
 	{
 		 this.showAlertIncorrect();
 		console.log('x masuk kosong je');
 	}
  }

  doRegister()
  {
  	this.navCtrl.push(SignupPage);
  }

  showAlertIncorrect() 
  {
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: 'Incorrect Username or Password',
      buttons: ['OK']
    });
    alert.present();
  }

  doForget()
  {
    console.log("forgeeeeeeeeeeeeeeeeeeeeet");

    let modal = this.modalCtrl.create(ForgetPage);
    modal.present();
  
  }
}
