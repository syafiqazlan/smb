import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
/**
 * Generated class for the ForgetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forget',
  templateUrl: 'forget.html',
})
export class ForgetPage {

  datas = {};

  constructor( public alertCtrl: AlertController, public navCtrl: NavController, public http: Http, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgetPage');
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }

  goSend()
  {
  	    var receiver = this.datas['email'];

		this.http.get('http://localhost/smb/checkEmail.php?email='+receiver).map(res2 => res2.json()).subscribe(data2 => {
		var status = data2[0].status;
		if (status == "exist")
		{
			console.log("email exist");

			//send temp password to email
			this.http.get('https://syafiqevo.000webhostapp.com/11/sendMail.php?receiver='+receiver).map(res1 => res1.json()).subscribe(data1 => {
			var tempPassword =  data1[0].tempPassword;

			//update database
			this.http.get('http://localhost/smb/setTempEmail.php?email='+receiver+'&tempPassword='+tempPassword).map(res => res.json()).subscribe(data => {
			// var tempPassword =  data1[0].tempPassword;

			
			});

			});

							    let alert = this.alertCtrl.create({
				      title: 'Success',
				      subTitle: 'A temporary password has been send to your email account',
				      buttons: ['OK']
				    });
				    alert.present();
						}

		else
		{
			console.log("email not exist");
										    let alert = this.alertCtrl.create({
				      title: 'Failed',
				      subTitle: 'You have not registered your email account to SMB Apps',
				      buttons: ['OK']
				    });
				    alert.present();
						
		}

 });
  }
}
