import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the DetailModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-modal',
  templateUrl: 'detail-modal.html',
})
export class DetailModalPage {

  billId:any;
  billMembers:any;
  amountEach:any;
  billType:any;
 amountPaid:any; 
  datas = {};
  userId:any;

  constructor(public navCtrl: NavController,  public http: Http, public navParams: NavParams,  public storage: Storage, public viewCtrl: ViewController) {
 
     this.billId = this.navParams.get('billId');
     this.billType = this.navParams.get('billType');
     this.amountEach = this.navParams.get('amountEach');
       //get bill members
     this.http.get('http://localhost/smb/getGroupMembers.php?billId='+this.billId).map(res2 => res2.json()).subscribe(data2 => {
     this.billMembers  = data2;  

});

     //get amount paid and amount each
     //get userId
        this.storage.get('userData').then((val) => {
          this.userId = val.userId;
     this.http.get('http://localhost/smb/getBillForPay.php?userId='+val.userId+'&billId='+this.billId).map(res2 => res2.json()).subscribe(data3 => {
     this.amountPaid  = data3[0].amountPaid;  
    });      
        });   

      this.userId = storage.get('userData')
.then( res => console.log(res.userId));

        //console.log("uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuid", this.userId); 

  }

// getUserId()
// {
//         this.storage.get('userData').then((val) => {
//           this.userId = val.userId;  
//                   return this.userId;   
//         });  

// }
  showPay()
  {
      console.log("uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuid", this.userId); 
    var payOption = this.datas['payOption'];
    var friend = this.datas['friend'];
    var amount = this.datas['amount'];
    var time = this.datas['time'];
    var total;

    if(payOption == "self")
    {
      if (amount > this.amountEach || amount > (this.amountEach-this.amountPaid) && this.billType=="one-off")
      {
        console.log("xleh byr lebig~~each:"+this.amountEach+'~~nk bayar:'+amount+'~~~dh byr:'+this.amountPaid);
      }
      else
      {
        if(time != undefined)
        {
         total = (parseInt(amount)*time) + parseInt(this.amountPaid); 
        console.log(amount+'~'+time+'~'+total); 
        this.http.get('http://localhost/smb/payBill.php?amountPaid='+total+'&userId='+this.userId+'&billId='+this.billId).map(res2 => res2.json()).subscribe(data2 => {
     this.billMembers  = data2;  
      });          
        }
        else
        {
         total = parseInt(amount) + parseInt(this.amountPaid); 
        console.log(amount+'~'+total); 
             this.http.get('http://localhost/smb/payBill.php?amountPaid='+total+'&userId='+this.userId+'&billId='+this.billId).map(res2 => res2.json()).subscribe(data2 => {
     this.billMembers  = data2;  

});         
        }
    
      }
    }
    else if (payOption == "friend")
    {
      if(time != undefined)
        {
         total = (parseInt(amount)*time) + parseInt(this.amountPaid); 
        console.log(amount+'~'+time+'~'+total+'~'+friend); 
             this.http.get('http://localhost/smb/payBill.php?amountPaid='+total+'&userId='+friend+'&billId='+this.billId+'&paidBy='+this.userId).map(res2 => res2.json()).subscribe(data2 => {
     this.billMembers  = data2;  

});          
        }
        else
        {
         total = parseInt(amount) + parseInt(this.amountPaid); 
        console.log(amount+'~'+total+'~'+friend);  
             this.http.get('http://localhost/smb/payBill.php?amountPaid='+total+'&userId='+friend+'&billId='+this.billId+'&paidBy='+this.userId).map(res2 => res2.json()).subscribe(data2 => {
     this.billMembers  = data2;  

});        
        }
    }
  }

onInputTime(data) : void {
  console.log("dataaa vhg", data);        
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailModalPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
