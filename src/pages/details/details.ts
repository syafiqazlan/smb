import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController, LoadingController } from 'ionic-angular';
import { Chart } from 'chart.js';
import { DetailModalPage } from '../detail-modal/detail-modal';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
/**
 * Generated class for the DetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-details',
  templateUrl: 'details.html',
})
export class DetailsPage {

  billAdmin:any;
  billSize:any;
  billTitle:any;
  billId:any;
  billAdminName:any;
  billMembers:any;
  totalPaid:any;
  totalPerc:any;
  total:any;
  userId:any;
  amountEach:any;
  due:any;
  target:any;
  billType:any;

  // Doughnut
 public barChartLabels:string[];
  public barChartType:string = 'bar';
  public barChartLegend:boolean = true;
 public barChartOptions:any = {
scales: {
            xAxes: [{
                stacked: true
            }],
            yAxes: [{
                stacked: true
            }]
        }
  };

  public barChartData:any[];

  // public barChartData:any[] = [
  //   {data: [65, 59, 80], label: 'Paid'},
  //   {data: [28, 48, 40], label: 'Unpaid'}
  // ];

 public stats;
  perct:any;
// events
public chartClicked(e:any):void {
  console.log(e);
}

public chartHovered(e:any):void {
  console.log(e);
}

  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, public http: Http, public navParams: NavParams, public modalCtrl: ModalController, public alertCtrl: AlertController,  public storage: Storage) {

   //   {data: [65, 59, 80], label: 'Paid'},
  //   {data: [28, 48, 40], label: 'Unpaid'}

       this.perct = "10";
     this.stats="track";

     this.billId = this.navParams.get('billId');
     console.log("selected id", this.billId);
     this.billAdmin = this.navParams.get('billAdmin');
     this.billSize = this.navParams.get('billSize');
     this.billTitle = this.navParams.get('billTitle');

     //get userId
        this.storage.get('userData').then((val) => {
        this.userId = val.userId;   
                console.log("Bill admin + current user"+this.billAdmin+this.userId);       
        });    

    //get admin name
  let loading = this.loadingCtrl.create({
    content: 'Loading...'
  });

  loading.present();

  setTimeout(() => {
    loading.dismiss();
  }, 3000);

     this.http.get('http://localhost/smb/getUsername.php?userId='+this.billAdmin).map(res => res.json()).subscribe(data => {
     this.billAdminName  = data[0].username;
});


    //get bill due, amount, amount each
     this.http.get('http://localhost/smb/getBillGroup.php?billId='+this.billId).map(res => res.json()).subscribe(data => {
     this.target  = data[0].billAmount;
     this.amountEach  = data[0].amountEach;
     this.due  = data[0].billDue;
     this.billType = data[0].billType;



       //get bill members
     this.http.get('http://localhost/smb/getGroupMembers.php?billId='+this.billId).map(res2 => res2.json()).subscribe(data2 => {
     this.billMembers  = data2;  



     //paid each

     var paid:number[] = new Array()  
     var unpaid:number[] = new Array()  
     var names:string[] = new Array()

    var total = 0;
    for(var i=0; i < data2.length; i++) {
      total += parseInt(data2[i].amountPaid);
      paid[i]= parseInt(data2[i].amountPaid);
      unpaid[i]= data2[i].amountEach - data2[i].amountPaid;
      names[i]=data2[i].username;
    }
    console.log("current total aidddd: ", total);
    this.total = total;

      this.barChartData = [
    {data: paid, label: 'Paid'},
    {data: unpaid, label: 'Unpaid'}
  ];

    this.barChartLabels = names;
    // //each
    // var total2 = 0;
    // for(var i2=0; i2 < data2.length; i2++) {
    //   total2 += parseInt(data2[i2].amountEach);
    // }

    this.totalPaid = total/this.target;
    this.totalPerc = Math.round(this.totalPaid*100);
});
      });       // loading.dismiss();
  }

   showPrompt() {
    let prompt = this.alertCtrl.create({
      title: 'Send Message',
      // message: "Enter a name for this new album you're so keen on adding",
      inputs: [
        {
          name: 'title',
          placeholder: 'Enter title'
        },
        {
          name: 'details',
          placeholder: 'Message details'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Send',
          handler: data => {
            // console.log("title + details+++"+data.title+data.details);
            var title = data.title;
            var details = data.details;
            var billId = this.billId;
     //get userId
        this.storage.get('userData').then((val) => {
  
  var date = new Date();
var FromDate =  ('0' + date.getDate()).slice(-2) + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' +date.getFullYear();        
             this.http.get('http://localhost/smb/sendMessage.php?mTitle='+title+'&mDetails='+details+'&userId='+val.userId+'&mDate='+FromDate+'&billId='+billId).map(res => res.json()).subscribe(data => {
            console.log("title + details+++"+title+details);
      if (data[0].status == "pass")
          {
            console.log('pass');
          }

      else
          { 
            console.log('x pass');
          }
});
      
        });    


          }
        }
      ]
    });
    prompt.present();
  }

  showPay()
  {
    console.log("bill typeeeeeeeeeee", this.billType);

    let modal = this.modalCtrl.create(DetailModalPage,
      {
        billId : this.navParams.get('billId'),
        amountEach : this.amountEach,
        billType: this.billType
      });
    modal.present();
  }

   getCode() {
     var id = this.navParams.get('billId');
    let alert = this.alertCtrl.create({
      title: 'Bill Code',
      subTitle: 'Code: BS01'+id+' <br/> Give your friend this code to invite them ',
      buttons: ['Ok']
    });
    alert.present();
  }
}
