import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { Storage } from '@ionic/storage';

import { ListPage } from '../pages/list/list';

import { WelcomePage } from '../pages/welcome/welcome';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { CreatePage } from '../pages/create/create';
import { DetailsPage } from '../pages/details/details';
import { ProfilePage } from '../pages/profile/profile';
import { MessagePage } from '../pages/message/message';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = WelcomePage;
  messageCount:any;

  username : any;
  email : any;

  private profilePage;
  private messagePage;
  private homePage;

  constructor(public events: Events,  public http: Http, public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,  public storage: Storage) {
    
    //this.rootPage = HomePage;

    this.homePage = HomePage;
    this.profilePage = ProfilePage;
    this.messagePage = MessagePage;

    this.initializeApp();
    this.initializeUserData();

  events.subscribe('user:check', () => {
    this.initializeUserData();
  });

  }

  initializeUserData()
  {
        this.storage.get('userData').then((val) => {
          if(val == null)
          {
            console.log("local NULLL");
          }

          else{
        console.log('userData username', val.username);
        console.log('userData email', val.email);

     this.http.get('http://localhost/smb/getMessage.php?userId='+val.userId).map(res => res.json()).subscribe(data => {

      if (data && data.length > 0) {
          // do something
          this.messageCount = data[0].length;
      } else {
          this.messageCount = 0;
      }

});
        this.username = val.username;
        this.email = val.email;            
          }

        });           

  }

  doLogout()
  {
     this.storage.clear();
     //window.location.reload();
    console.log("keluar");
    this.nav.push(LoginPage);
  }


  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
            // this.nav.push(page);
            //          this.nav.setRoot(page);
    this.rootPage = page;
  }
}
